﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _what-does-it-do:

What does it do?
================

This extension provides content elements special for the themes content elements. At the moment, we only offer flux_ ce's.

.. _flux: https://extensions.typo3.org/extension/flux/


.. figure:: ../Images/ce1.png
	:width: 574px
	:alt: Materializecss Content Elements

.. figure:: ../Images/ce2.png
	:width: 1000px
	:alt: Materializecss Backend
