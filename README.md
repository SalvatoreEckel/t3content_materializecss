# EXT:t3content_materializecss #

This extension provides flux content elements for the t3cms theme Materializecss

### General Information ###

* t3content_materializecss
* v2.3.1

### How do I get set up? ###

* Install the extension from TER
* Make sure **EXT:t3themes_materializecss** is loaded
* Make sure **EXT:flux** is loaded


* Have fun with your new content elements.

### Contribution guidelines ###

* We are happy, when you contribute with us!
* Merge requests are wished,
* PSR-2 guidlines

### Who do I talk to? ###

* Salvatore Eckel
* salvaracer@gmx.de