<?php

# Extension Manager/Repository config file for ext: "t3content_materializecss"

$EM_CONF[$_EXTKEY] = [
    'title' => 'TYPO3 Content - Materializecss',
    'description' => 'Provides flux fluid content elements for the t3cms theme Materializecss',
    'category' => 'templates',
    'author' => 'Salvatore Eckel',
    'author_email' => 'salvaracer@gmx.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '2.3.1',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.18-8.99.99',
        ],
        'conflicts' => [],
        'suggests' => [
            't3themes_materializecss' => '1.1.0',
        ],
    ],
];
